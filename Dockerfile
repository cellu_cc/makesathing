#Dockerfile for testing

# command procedure:
#     docker build -t image1 .
#     docker run -it --name image2 image1 /bin/bash
# makesathing is now in the /app folder

FROM python:2.7-wheezy

RUN mkdir /app

COPY . /app