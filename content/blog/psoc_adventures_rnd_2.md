Title: Adventures in PSoC, Round 2
Date: 2017-09-23 19:07
Category: PSoC
Tags: embedded
Slug: psoc-adventures-rd-2
Authors: Daniel Cellucci
Summary: Daniel tries to make the PSoC breathe

PSoC is pretty daunting as a platform- it uses it's own software (which only runs on Windows :/ ), so you're not only learning a new chip but also a whole new IDE. I was looking through PSoC tutorials and I found the most basic one. Combining two PWM UDBs together with an XOR, the PSoC creates a PWM 

My first attempt at doing it on my own was to try to change the PWM duty cycle with a triangular wave. That's apparently not the right way to do it- you can't access the duty cycle registers from the UDB menu, which means you can't change them during runtime (this way, I guess).

![This is the plan]({filename}../media/psoc_adventures/PWM_XOR.jpg)
[source](http://www.cypress.com/documentation/code-examples/ce203303-psoc-3-and-psoc-5lp-breathing-led)

Anyway, the [getting started](http://www.cypress.com/file/41436/download) did a great job of taking me through setting things up (thanks Nidhin!). The basic premise is to run two PWM blocks at a 50% duty cycle with slightly different frequencies. The two square waves then interfere with one another, creating a wave whose duty cycle oscillates between 0 and 100%. This oscillating wave is then connected to the PWM module, producing the breathing effect. 

I found the whole setup straightforward, including the specification of the off-chip elements like the resistor and LED, though I'm not entirely sure they are necessary. After setting up the whole thing, however, I noticed that the PWM frequency was too low to get a clean breathing behavior. You can see this in the following gif.

![flickering]({filename}../media/psoc_adventures/Breathe_Original.gif)

It doesn't look so good, so I decided to give improving things a shot. The first thing I tried was just increasing the resolution of the PWM. The UDB setup has options to make the PWM 16-bit rather than 8-bit, giving us many (256 times) more grades to choose from between 0 and 100% duty cycle. I decided to keep the same relative frequences as specified in the getting started document, but I had to increase the clock that fed the PWM modules by a factor of 1000, from 5 KHz to 5 MHz, in order to make it breathe at a similar frequency to the previous 

![16-bit]({filename}../media/psoc_adventures/Breathe_16bit.gif)

However 5 MHz clocks and 16-bit timers isn't really in keeping with the spirit of embedded programming- our lives should be more resource constrained than that. As a result, I took a look at the underlying math that makes the breathe behavior work.

![beats]({filename}../media/psoc_adventures/beats.gif)
[Source](http://www.acs.psu.edu/drussell/Demos/superposition/superposition.html)

You can think of the two PWM square waves as two sine waves that have slightly different frequencies. For example

$$
\begin{eqnarray}
y_1(t) &=& sin(\omega_1 t)\\
y_2(t) &=& sin(\omega_2 t)\\
\end{eqnarray}
$$

The superposition of these two waves is then

$$
(y_1\wedge y_2)(t) = 2 \textrm{cos}(\frac{\omega_1-\omega_2}{2} t) \textrm{sin}(\frac{\omega_1+\omega_2}{2} t)
$$

Where $\wedge$ is the `XOR` operation, $(\omega_1+\omega_2)/2$ is the frequency of the internal wave, and $(\omega_1-\omega_2)/2$ is the envelope. The closer in frequency the two base waves, the longer the length of the breath. 

So, spoiler alert: the reason why the 16-bit counter worked at all was because it was running at 5 MHz, not because it was 16-bits. That is, all we need to do for this wave is increase the speed of the clock so that the full breathing motion is outside the range that human eyes can discern. However, just increasing the clock without changing anything else will produce a faster pulse. If we want to increase the clock and still get the breathe frequency we want, we also have to make sure the two PWM waveforms are closer together. This makes the envelope waveform longer, and therefore keeps the overall breathing frequency at the right speed.

I ended up running the system at 25 KHz instead of 5, and putting the frequency of the two 8-bit counters as close as possible together. This produced a waveform that was sufficiently high PWM frequency that there was no more flickering, but still small enough envelope frequency that the breathing waveform still looked the way I wanted. You can see the result below. 

![8-bit]({filename}../media/psoc_adventures/Breathe_8Bit.gif)

## Next Time 
I'll start looking at making my own UDBs. They look confusing. 


