Title: Adventures in PSoC, Round 1
Date: 2017-09-17 9:33
Category: PSoC
Tags: embedded
Slug: psoc-adventures-rd-1
Authors: Daniel Cellucci
Summary: Daniel tries to make the PSoC blink

So over the past few months (years) I've gotten pretty familiar with the Arduino IDE, as well as the 328 and now the SAMD21 architectures. It's been fun, but there's also some pain points (like the lack of a debugger) that have made the process sometimes super tedious.

With that in mind, I decided to explore what working with other architectures/toolchains might be like. The first of these was Cypress' PSoC.

## Why PSoC

I picked PSoC because I liked the idea of the Universal Digital Block (UDB). A UDB is a module of reconfigurable logic designed to act like a small ASIC. What this means is that they can perform complex behaviors without CPU intervention, freeing up time for more general calculations on the CPU. Since I'm coming from the Arduino Zero/ Atmel SAM world, I am viewing the UBDs as the next logical step after SERCOMs.

## What I have in mind

At some point, I will document the projects that led to this one, OuroboroSat and MADCATV1, but the vision is this: 

Part of the value of digital cellular solids is the decomposition of a complex structure into many identical building blocks. This simplifies the manufacturing, since you only have to design one block, and it can even simplify the structural simulation, since the finite elements that compose the simulation can be the size of the block instead of a small microstructure. 

My work has been exploring how the building-block approach can be applied to electronic sensor systems. Namely, I look at how we can augment a digital cellular solids structure with electronics capable of sensing and reporting on their environment, and I try to figure out how we can use qualities like periodicity to simplify the code and algorithms that run on these modules. One way I've done that is by designing generalized modules that communicate with one another using addressless algorithms. Another is that I work with really smart controls people like Nick Cramer who look at how assumptions about the geometry of a lattice can simplify the models that the modules can use to filter raw sensor data.

You can see why, from this description, a PSoC might be a good choice- modular sensor networks meets modular processor architecture.

## But First...

I'm going to PWM an LED using PSoC Creator, so let's get into it. 
